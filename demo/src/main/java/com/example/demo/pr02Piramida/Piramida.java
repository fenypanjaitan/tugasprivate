package com.example.demo.pr02Piramida;

import java.util.Scanner;

public class Piramida {
    public static void main(String[] args) {
        Piramida piramida = new Piramida();
        piramida.segitiga();
    }

    public void segitiga(){
        Scanner scanner = new Scanner(System.in);
        System.out.print("masukkan jumlah bintang: ");
        int bintang = scanner.nextInt();

        for (int i = 0; i <=bintang; i++) {
            for (int j = bintang; j >i-1 ; j--) {
                System.out.print(" ");
                }
            for (int k = 0; k <i-1 ; k++) {
                System.out.print("*");
            }
            for (int j = 0; j <i ; j++) {
                System.out.print("*");
            }
            System.out.println();
        }
    }
}
