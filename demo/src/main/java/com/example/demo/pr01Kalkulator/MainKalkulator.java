package com.example.demo.pr01Kalkulator;

import java.util.Scanner;

public class MainKalkulator{
    public static void main(String[] args) {
        float x, y;

        Scanner scanner = new Scanner(System.in);
        System.out.print("masukkan nilai x: ");
        x = scanner.nextFloat();
        System.out.print("masukkan nilai y: ");
        y = scanner.nextFloat();

        System.out.println("Pilih operasi yang hendak dilakukan: ");
        System.out.println("[1].tambah \n[2].kurang \n[3].kali \n[4].bagi\n[5].modulus\n");
        int pilihan= scanner.nextInt();

        switch (pilihan){
            case 1:
                float tambah = x+y;
                System.out.println(tambah);
                break;
            case 2:
                float kurang = x-y;
                System.out.println(kurang);
                break;
            case 3:
                float kali = x*y;
                System.out.println(kali);
                break;
            case 4:
                float bagi = x/y;
                System.out.println(bagi);
                break;
            case 5:
                float modulus = x%y;
                System.out.println(modulus);
                break;
            default:
                System.out.println("pilih yang benar ");

        }
    }


}
