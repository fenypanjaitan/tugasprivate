package com.example.demo.pr03NamaBintang;

public class HurufBintang {
    int height = 5;

    protected void printF() {
        for (int i = 0; i < height; i++) {
            System.out.print("*");
            for (int j = 0; j < height; j++) {
                if ((i == 0) || (i == height / 2 && j <= height / 2)) {
                    System.out.print("*");
                }
            }
            System.out.println();
        }
        System.out.println();
    }


    protected void printE(){
        for (int i = 0; i <height ; i++) {
            System.out.print("*");
            for (int j = 0; j <height ; j++) {
                if( (i==0 || i==height-1) || (i==height/2 && j<=height/2)){
                    System.out.print("*");
                }
            }
            System.out.println();
        }
        System.out.println();
    }

    protected void printN(){
        int counter = 0;
        for (int i = 0; i <height ; i++) {
            System.out.print("*");
            for (int j = 0; j <=height ; j++) {
                if(j==height) {
                    System.out.print("*");
                }
                else if(j==counter){
                    System.out.print("*");
                } else{
                    System.out.print(" ");
                }
            }
            counter++;
            System.out.println();
        }
        System.out.println();
    }

    protected void printY(){
        int counter =0;
        for (int i = 0; i <height ; i++) {
            for (int j = 0; j <= height; j++) {
                if(j==counter||j==height-counter && i<=height/2){
                    System.out.print("*");
                } else {
                    System.out.print(" ");
                }
            }
            System.out.println();
            if (i<height/2){
                counter++;
            }

        }
    }

}
