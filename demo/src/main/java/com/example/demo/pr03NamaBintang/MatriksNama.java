package com.example.demo.pr03NamaBintang;

public class MatriksNama {
    public static void main(String[] args) {
        int a,b;
        int [][] matriks ={
                {1,1,1,1,1, 0,0, 1,1,1,1,1, 0,0, 1,1,0,0,0,0,1, 0,0, 1,0,0,0,1},
                {1,0,0,0,0, 0,0, 1,0,0,0,0, 0,0, 1,0,1,0,0,0,1, 0,0, 0,1,0,1,0},
                {1,1,1,1,0, 0,0, 1,1,1,1,0, 0,0, 1,0,0,1,0,0,1, 0,0, 0,0,1,0,0},
                {1,0,0,0,0, 0,0, 1,0,0,0,0, 0,0, 1,0,0,0,1,0,1, 0,0, 0,0,1,0,0},
                {1,0,0,0,0, 0,0, 1,1,1,1,1, 0,0, 1,0,0,0,0,1,1, 0,0, 0,0,1,0,0}
        };

        for (a = 0;  a<5; a++) {
            for (b=0;b<28;b++){
                if (matriks[a][b] == 1){
                    System.out.print("*");
                }else if(matriks[a][b]==0){
                    System.out.print(" ");
                }
            }
            System.out.println();
        }
    }
}
